<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true){die();}?>
<form action="" id="nanweb_geoip" class="nanweb_geoip">
	<div class="input-group">
		<input type="text" name="ip" placeholder="<?=GetMessage("NANWEB_PLACEHOLDER") ?>" class="form-control">
		<button class="btn btn-primary btn-md" type="submit"><?=GetMessage("NANWEB_BTN_SUBMIT") ?></button>
	</div>
	<div id="nanweb_message"></div>
</form>