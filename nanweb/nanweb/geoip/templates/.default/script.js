document.addEventListener("DOMContentLoaded", function(event) {
	document.getElementById("nanweb_geoip").addEventListener('submit', function(e) {
	  e.preventDefault();
		BX.ajax.runComponentAction('nanweb:geoip',
			'validation', {
			  mode: 'class',
			  data: new FormData(this),
			})
			.then(function (response) {//без ошибок
				document.getElementById("nanweb_message").innerText = response.data;	
			}, function (response) {//ошибки
				document.getElementById("nanweb_message").innerText = response.errors[0].message;
			});
	});
});
