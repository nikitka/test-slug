<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
	"NAME" => GetMessage("NANWEB_NAME"),
	"DESCRIPTION" => GetMessage("NANWEB_DESC"),
	"PATH" => array(
		"ID" => "NaNweb",
		"NAME" => "NaNweb",
	),
);

?>