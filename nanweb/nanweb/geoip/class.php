<?php

use Bitrix\Main;
use \Bitrix\Main\Web\HttpClient;
use Bitrix\Main\Error;
use Bitrix\Main\ErrorCollection;
use Bitrix\Highloadblock as HL;
use Bitrix\Main\Entity;
use Bitrix\Main\Engine\Contract\Controllerable;
use Bitrix\Main\Mail\Event;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true){die();}


class NaNwebGeoComponent extends \CBitrixComponent implements Controllerable{
	//TODO вынести в настройки компонента
	public $hlId = 4;
	
    public function configureActions()
    {
       return [
            'validation' => [
                'prefilters' => [],
            ],
        ];
    }
	
	/*
	*	Валидация при ajax запросе
	*/
    public function validationAction($ip)
    {
		if (filter_var($ip, FILTER_VALIDATE_IP) === false) {
			throw new Exception('Не верно введен IP');
		} else {
			if($getHl = $this->getHL($ip)){//Проверяем в HL блоке
				$return = $getHl['UF_OTVET'];
			} else {						
				$return = $this->getIP($ip);//делаем запрос к sypexgeo
			}
		}
        return $return;
    }
	
	/*
	*	Получаем сущность
	*/
    private function getEntity()
    {
		\CModule::IncludeModule('highloadblock');
	   $hlblock = HL\HighloadBlockTable::getById($this->hlId)->fetch();
	   $entity = HL\HighloadBlockTable::compileEntity($hlblock);
	   $entityClass = $entity->getDataClass();
	   return $entityClass;
    }
	
	/*
	*	Ищем IP в HL блоке 
	*/
    private function getHL($ip)
    {
	   $entityClass = $this->getEntity();
	   $response = $entityClass::getList(array(
		   'select' => array('*'),
		   'filter' => array('=UF_IP' => $ip)
	   ))->fetch();
	   return $response;
    }
	
	/*
	*	Ищем IP в sypexgeo.net ,
	*	если error отправляем письмо и отдаем ошибку
	*	если ответ не error пишем в HL блок и отдаем
	*/
    private function getIP($ip)
    {
		$httpClient = new HttpClient(["waitResponse" => true]);
		$res = $httpClient->get("https://api.sypexgeo.net/json/" . $ip);
		$response = json_decode($res, true);

		if($response['error']){
			Event::sendImmediate(array(
				"EVENT_NAME" => "ERROR_NOTIFICATION", 
				"LID" => SITE_ID, 
				"C_FIELDS" => array( 
					'MESSAGE'=>$response['error']
				), 
			));
			throw new Exception($response['error']);
		} else {
			$entityClass = $this->getEntity();
			$entityClass::add(['UF_IP'=>$ip,'UF_OTVET'=>$res]);
		}
	   return $res;
    }
	
	
    public function executeComponent()
    {
        $this->includeComponentTemplate();
		return parent::executeComponent();
    }
}